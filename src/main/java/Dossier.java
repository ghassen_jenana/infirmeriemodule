import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;


@Entity
public class Dossier {


    @Id
    Integer id ;

   // Etudiant etudiant ;

    Date date_dossier ;

    String description ;

//    public Etudiant getEtudiant() {
//        return etudiant;
//    }
//
//    public void setEtudiant(Etudiant etudiant) {
//        this.etudiant = etudiant;
//    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    @Override
    public String toString() {
        return "Dossier{" +
                "id=" + id +
                '}';
    }
}
